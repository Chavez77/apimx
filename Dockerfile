# Imagen BAse

FROM node:latest

# Carpeta raiz contenedor

WORKDIR /app

#Copiado de archivos

ADD . /app

#Dependencias

RUN npm install

# Puerto que exponemos
EXPOSE 3000

#Comandos para ejecutar la aplicacion
CMD ["npm","start"]