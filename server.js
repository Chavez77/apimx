var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjason = require('request-json');

var path = require('path');

var movimientosJSON = require('./movimientosV2.json');

var urlClientes = 'https://api.mlab.com/api/1/databases/achavezb/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var clienteMlab = requestjason.createClient(urlClientes)

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function (req,res,next) {
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Origin","Origin","X-Requested-With","Accept");
    next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function (req,res) {
    //res.send("Hola mundo :)");
    res.sendfile(path.join( __dirname,"index.html"));
})

app.post('/',function (req,res) {
    res.send("Hemos recibido su petición POST :)");

})

app.put('/',function (req,res) {
    res.send("Hemos recibido su petición PUT cambiada :)");

})

app.delete('/',function (req,res) {
    res.send("Hemos recibido su petición POST :)");

})

app.get('/clientes/:idCliente',function (req,res) {
    res.send("Cliente número: " + req.params.idCliente + " :)");

})

app.get('/v1/movimientos',function (req,res) {
    res.sendfile("movimientosV1.json");

})

app.get('/v2/movimientos2',function (req,res) {
    res.json(movimientosJSON);

})

app.get('/v2/movimientos/:idCliente',function (req,res) {
    res.send(movimientosJSON[req.params.idCliente]);

})

app.get('/v2/movimientosQ',function (req,res) {
    var aux = req.query.id;
    res.send(movimientosJSON[aux]);
})

app.post('/v2/movimientos',function (req,res) {
    var nuevo = req.body;
    nuevo.id = movimientosJSON.length + 1;
    movimientosJSON.push(nuevo);
    //res.send("Movimiento dado de alta");
    res.json(movimientosJSON);

})

app.get('/Clientes',function (req,res) {
    clienteMlab.get('',function (err,resM,body) {
        if(!err){
            res.send(body);
        }else {
            console.log(body);
        }
    })

})

app.post('/Clientes',function (req,res) {
    clienteMlab.post('', req.body,function (err,resM,body) {
            res.send(body);
    })

})
